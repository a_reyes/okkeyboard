/***********************************************************************
 * Short click -> sends down arrow
 * Long click -> sends enter
 ***********************************************************************/

#include <Arduino.h>
#include "USB.h"
#include "USBHIDKeyboard.h"

#include <Adafruit_NeoPixel.h>

#define PIN        48
#define NUMPIXELS 1


Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
#define DELAYVAL 500

USBHIDKeyboard Keyboard; /* https://github.com/espressif/arduino-esp32/blob/master/libraries/USB/examples/Keyboard/KeyboardSerial/KeyboardSerial.ino */

const int PIN_BUTTON = 0;              /* Button BOOT */
const uint32_t LONG_CLICK_TIME = 1000; /* Long click if button pressed for longer than 1 second */
uint32_t button_init_time = 0;
uint32_t button_release_time = 0;

bool button_is_released();
bool is_long_click();

/***********************************************************************
 * @brief Returns true after button is released
 *
 * @return true
 * @return false
 ***********************************************************************/
bool button_is_released()
{
  bool is_released = false;
  static bool is_pressed = false;
  static const int DEBOUNCE_DELAY = 10;

  if (digitalRead(PIN_BUTTON) == LOW)
  {
    if (is_pressed == false)
    {
      delay(DEBOUNCE_DELAY);
      if (digitalRead(PIN_BUTTON) == LOW)
      {
        button_init_time = millis();
        is_pressed = true;
      }
    }
  }
  else
  {
    if (is_pressed == true)
    {
      delay(DEBOUNCE_DELAY);
      if (digitalRead(PIN_BUTTON) == HIGH)
      {
        button_release_time = millis();
        is_pressed = false;
        is_released = true;
      }
    }
  }

  return is_released;
}

/***********************************************************************
 * @brief Returns true if button is pressed for long time (> 1 second)
 *
 * @return true
 * @return false
 * **********************************************************************/
bool is_long_click()
{
  if ((button_release_time - button_init_time) > LONG_CLICK_TIME)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void setup()
{
  Serial.begin(115200);
  Keyboard.begin();
  USB.begin();
  pinMode(PIN_BUTTON, INPUT);

  
  pixels.begin();
  
  pixels.setBrightness(5);
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(0, 0, 255));
  pixels.show();

  delay(5000);
  if (Keyboard.is_ready() == true)
  {
    pixels.setPixelColor(0,pixels.Color(0,255,0));
    pixels.show();
  }
}

void loop()
{

  if (button_is_released())
  {
    if (Keyboard.availableForWrite() <= 0) 
    {
      Keyboard.end();
      Keyboard.begin();
    }
    if (is_long_click())
    {
      Keyboard.write(KEY_RETURN);
    }
    {
      Keyboard.write(KEY_DOWN_ARROW);
    }
  }
}
